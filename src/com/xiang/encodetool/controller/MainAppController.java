package com.xiang.encodetool.controller;

import com.xiang.encodetool.service.Handler;
import com.xiang.encodetool.util.TypeConst;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.collections.FXCollections;

/**
 * Created by xiangliyou on 17-8-5.
 *
 * @DESCRIPTION
 */
public class MainAppController {

    private Handler handler;

    static ObservableList<String> options =
            FXCollections.observableArrayList(
                    "Option 1", "Option 2", "Option 3" );

    @FXML
    private static ChoiceBox<?> choiceEncodeCB;
    @FXML
    private ListView<String> fileListView;
    @FXML
    private ProgressBar progressBar;

    public MainAppController() {
//        choiceEncodeCB.setItems(FXCollections.observableArrayList(
//                "GBK to UTF-8", "UTF-8 to GBK"
//        ));
//        choiceEncodeCB.getSelectionModel().selectedIndexProperty().addListener(
//                (observable, oldValue, newValue) -> handler.setEncoedType(observable.getValue().toString()));
    }

    /**
     * 选择文件
     * @param actionEvent 事件源
     */
    @FXML
    public void choiceFile(ActionEvent actionEvent) {
        String[] fileNames =  handler.choiceFile();
        //将得到的列表显示在列表框
        showFileStringList(fileNames);
    }



    /**
     * 开始转换
     * @param actionEvent 事件源
     */
    @FXML
    public void start(ActionEvent actionEvent) {
        Task task = handler.start(TypeConst.TO_UTF8);
        //更新进度条显示
        progressBar.progressProperty().bind(task.progressProperty());
    }


    @FXML
    public void choiceDir(ActionEvent actionEvent) {
        String[] fileNames = handler.choiceDir();
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    private void showFileStringList(String[] fileNames) {
        ObservableList<String> listVIewItems = FXCollections.observableArrayList(fileNames);
        fileListView.setItems(listVIewItems);
    }
}
