package com.xiang.encodetool.view;/**
 * Created by xiangliyou on 17-8-5.
 *
 * @DESCRIPTION
 */

import com.xiang.encodetool.service.Handler;
import com.xiang.encodetool.controller.MainAppController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public class MainApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        //加载FXML资源文件
        FXMLLoader loader = new FXMLLoader(getClass().getResource("MainApp.fxml"));
        //配置舞台
        Pane mainPane = loader.load();
        primaryStage.setScene(new Scene(mainPane));
        primaryStage.setTitle("向向转码器");
        //为控制器设置处理事务的对象
        MainAppController controller = loader.getController();
        Handler handler = new Handler(primaryStage);
        controller.setHandler(handler);

        //拉开幕布，显示舞台
        primaryStage.show();
    }
}
