package com.xiang.encodetool.filter;


/**
 * Created by xiangliyou on 17-8-6.
 *
 * @DESCRIPTION 自定义的文件过滤器，排除不需要的文件
 */
public class MyFileFilter {


    /**
     * 非常简单的文件名过滤器，根据文件的后缀名过滤
     *
     * @param name 文件名
     * @return true是文本文件，false不是文本文件
     */
    public static boolean simpleNotTextFileFilter(String name) {
        boolean isTextFile = false;
        int expandedNameIndex = name.lastIndexOf(".");
        String expandedName = (String) name.subSequence(expandedNameIndex + 1, name.length());
        //转化为小写处理
        expandedName = expandedName.trim().toLowerCase();
        System.out.println("expandedName = " + expandedName);
        //没有后缀名，默认为文本文件
        if (expandedName.isEmpty()) {
            isTextFile = true;
        } else if (expandedName.contains("rar")) {

        } else if (expandedName.contains("zip")) {

        } else if (expandedName.contains("mp3")) {

        } else if (expandedName.contains("mp4")) {

        } else if (expandedName.contains("jpg")) {

        } else if (expandedName.contains("png")) {

        } else {
            isTextFile = true;
        }

        return isTextFile;
    }
}
